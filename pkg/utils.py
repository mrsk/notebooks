
import qiskit
import qiskit_textbook
from qiskit_textbook.tools import array_to_latex
#import math


def get_unitary(qc):
    backend = qiskit.Aer.get_backend('unitary_simulator')
    result = qiskit.execute(qc, backend).result().get_unitary()
    return result

def show_unitary(qc):
    unitary = get_unitary(qc)
    array_to_latex(unitary)

def get_statevector(qc):
    backend = qiskit.Aer.get_backend('statevector_simulator')
    result = qiskit.execute(qc, backend).result().get_statevector()
    return result

def show_statevector(qc):
    statevector = get_statevector(qc)
    return qiskit.visualization.plot_bloch_multivector(statevector)

def get_counts(qc):
    backend = qiskit.Aer.get_backend('qasm_simulator')
    result = qiskit.execute(qc, backend, shots=1000).result().get_counts()
    return result

def show_counts(qc):
    counts = get_counts(qc)
    return qiskit.visualization.plot_histogram(counts)