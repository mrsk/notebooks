

import qiskit
import math

def apply_grovers(
    qc,
    search_qbits,
    working_qbit,
    apply_oracle,
):
    iterations = int(2**(len(search_qbits) / 2) * math.pi / 4)

    # put search_qbits into equal superposition
    for qbit in search_qbits:
        qc.h(qbit)

    # put working qbit into '-' state
    qc.x(working_qbit)
    qc.h(working_qbit)
    
    for _i in range(iterations):
        apply_grovers_iteration(
            qc,
            search_qbits,
            working_qbit,
            apply_oracle
        )
    
    # uncomputation of working qbit
    qc.h(working_qbit)
    qc.x(working_qbit)


def apply_grovers_iteration(
    qc,
    search_qbits,
    working_qbit,
    apply_oracle
):
    
    apply_oracle(qc, search_qbits, working_qbit)
    
    for qbit in search_qbits:
        qc.h(qbit)
        qc.x(qbit)
    
    qc.mcx(search_qbits, working_qbit)
    
    for qbit in search_qbits:
        qc.x(qbit)
        qc.h(qbit)
        

def apply_zero_oracle(qc, search_qbits, working_qbit):
    for qbit in search_qbits:
        qc.x(qbit)
        
    qc.mcx(search_qbits, working_qbit)
    
    for qbit in search_qbits:
        qc.x(qbit)


def apply_ones_oracle(qc, search_qbits, working_qbit):
    qc.mcx(search_qbits, working_qbit)